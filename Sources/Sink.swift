import CPulse

public class Sink {

    public typealias SinkCallback = ()->()


    var unsafeSelf: UnsafeMutableRawPointer! = nil
    let context: Context
    let name: String

    public var info: pa_sink_info! 
    var index: UInt? = nil;

    var callback: SinkCallback = {}

    public init(_ sinkName: String,context ctx: Context, cb: @escaping SinkCallback = {}) {

        name = sinkName
        context = ctx
        callback = cb

        unsafeSelf = UnsafeMutableRawPointer(Unmanaged.passUnretained(self).toOpaque())

        refreshData()
        
        subscribeSinkEvent()
    }

    public func setCallback(cb: @escaping SinkCallback){
        callback = cb
    }



    func refreshData() {

        pa_context_get_sink_info_by_name(context.c_context, name, {
            context, sink_info, eol, data in

            let this = Unmanaged<Sink>.fromOpaque(data!).takeUnretainedValue()
            if eol != 0 {
                return
            }
            this.info = sink_info!.pointee

            this.dataReady()

        }, unsafeSelf)
    }

    func subscribeSinkEvent(){

        pa_context_set_subscribe_callback(context.c_context, {
            ctx, event_type, idx, data in

            let this = Unmanaged<Sink>.fromOpaque(data!).takeUnretainedValue()

            let facility = event_type.rawValue & PA_SUBSCRIPTION_EVENT_FACILITY_MASK.rawValue

            let isSinkEvent = PA_SUBSCRIPTION_EVENT_SINK == pa_subscription_event_type(facility)
            if(isSinkEvent) {
                this.refreshData()
            }
            // print(a)
        }, unsafeSelf)

        pa_context_subscribe(context.c_context, PA_SUBSCRIPTION_MASK_SINK, nil, nil)

    }

    func dataReady(){
        callback()
    }

    func setVolume(_ volume: inout CVolume) {
        if info == nil {
            return;
        }

        pa_context_set_sink_volume_by_index(context.c_context, info!.index, &volume, {
            context, success, data in
            print("volume set")
        }, nil);
    }
    public func setVolume(_ newVolume: Double) {
        var vol = info.volume
        vol.set(volume: newVolume)

        self.setVolume(&vol)
    }
}
