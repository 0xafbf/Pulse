import CPulse


public class MainLoopAPI {
    
    let c_mainloop_api: UnsafeMutablePointer<pa_mainloop_api>
    
    public init(_ mainLoop: MainLoop){
        c_mainloop_api = pa_mainloop_get_api(mainLoop.c_mainloop)
    }
}