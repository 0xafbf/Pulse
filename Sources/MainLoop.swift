import CPulse
import Dispatch

public class MainLoop {
    
    let c_mainloop:OpaquePointer
    
    let mainLoopQueue: DispatchQueue
    
    public init(){
        c_mainloop = pa_mainloop_new()
        
        mainLoopQueue = DispatchQueue(label: "pulse_queue", target: DispatchQueue.global())
        
    }
    
    public func run() {
        mainLoopQueue.async{
            pa_mainloop_run(self.c_mainloop, nil)
        }
    }
    
    public func quit() {
        pa_mainloop_quit(c_mainloop, 0)
    }
    
}

