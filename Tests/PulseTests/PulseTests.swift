import XCTest
@testable import Pulse

class PulseTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        XCTAssertEqual(Pulse().text, "Hello, World!")
    }

    func testPulseAudio() {
        
        let device = Device(client_name:"bemote")

        XCTAssertNotNil(device, "device is nil!")
        
    }


    static var allTests : [(String, (PulseTests) -> () throws -> Void)] {
        return [
            ("testExample", testExample),
            ("testPulseAudio", testPulseAudio),
        ]
    }
}
